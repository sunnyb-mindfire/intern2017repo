--
-- Table structure for table `sc_kb_last_check`
--

CREATE TABLE `sc_kb_last_check` (
  `id` int(11) NOT NULL,
  `limitation` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sc_kb_last_check`
--
ALTER TABLE `sc_kb_last_check`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sc_kb_last_check`
--
ALTER TABLE `sc_kb_last_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `sc_kb_last_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;