SELECT
  CONCAT_WS(
    ' ',
    emp.prefix,
    emp.firstName,
    emp.middleName,
    emp.lastName
  ) AS fullName,
  emp.emailID,
  emp.gender,
  CONCAT_WS(
    ', ',
    addr.type,
    addr.street,
    city.name,
    state.name,
    addr.zipCode
  ) AS fullAddress,
  CONCAT_WS(
    ' : ',
    contact.type,
    contact.number
  ) AS contactInfo,
  communication.medium AS prefCommMedium
FROM
  employee emp
LEFT JOIN
  address addr ON emp.PK_ID = addr.FK_employeeID
RIGHT JOIN
  city ON addr.FK_cityID = city.PK_ID
RIGHT JOIN
  state ON city.FK_stateID = state.PK_ID
LEFT JOIN
  contact ON emp.PK_ID = contact.FK_employeeID
LEFT JOIN
  employeeCommunication empComm ON emp.PK_ID = empComm.FK_employeeID
RIGHT JOIN
  communication ON empComm.FK_communicationID = communication.PK_ID